#! /usr/bin/env python
"""
    Prepare everything for a release of the software.

    1. update the version number in `version`
    2. (Re)build the code to include the new version
    3. Generate packages

    The updated version is passed via the command line.
    This script expects to be called from the repository root.

    Usage: prepare.py <Major.Minor.Patch>

    The script is run by semantic-release in the CI pipeline.
"""

import subprocess
import sys
from subprocess import CompletedProcess
from typing import List, Optional

import toml

VERSION_FILE = "pyproject.toml"


def execute_exit_on_failure(args: List[str], cwd: Optional[str] = None):
    result: CompletedProcess = subprocess.run(args, cwd=cwd, check=False)
    if result.returncode != 0:
        sys.exit(result.returncode)


def main():
    if len(sys.argv) < 2:
        print(__doc__)
        return 1

    new_version = sys.argv[1]

    print("Update version file to", new_version)
    with open(VERSION_FILE, "r", encoding="utf8") as file:
        data = toml.load(file)

    data["tool"]["poetry"]["version"] = new_version

    with open(VERSION_FILE, "w", encoding="utf8") as file:
        toml.dump(data, file)

    print("Build with new version")
    execute_exit_on_failure(["poetry", "install"])
    execute_exit_on_failure(["poetry", "build"])

    return 0


if __name__ == "__main__":
    sys.exit(main())
