#! /usr/bin/env python
"""
    Publish a new version of the software on PyPI.

    1. publish to pypi

    Usage: publish.py

    The script is run by semantic-release in the CI pipeline.
"""

import os
import subprocess
import sys
from subprocess import CompletedProcess
from typing import List, Optional


def execute_exit_on_failure(args: List[str], cwd: Optional[str] = None):
    result: CompletedProcess = subprocess.run(args, cwd=cwd, check=False)
    if result.returncode != 0:
        sys.exit(result.returncode)


def main():
    pypi_token = os.getenv("PYPI_REGISTRY_TOKEN")

    execute_exit_on_failure(["poetry", "config", "pypi-token.pypi", pypi_token])
    execute_exit_on_failure(["poetry", "publish"])

    return 0


if __name__ == "__main__":
    sys.exit(main())
