## [3.1.1](https://gitlab.com/derfreak/MediaScrapper/compare/v3.1.0...v3.1.1) (2022-12-30)


### Bug Fixes

* fix a crash where the tmdb api key is none ([34eee75](https://gitlab.com/derfreak/MediaScrapper/commit/34eee75b7b28800ad60b34c0357e6d76a0e2f576))

# [3.1.0](https://gitlab.com/derfreak/MediaScrapper/compare/v3.0.4...v3.1.0) (2022-12-30)


### Features

* acquire tmdb api key from mendia server ([fd65f8d](https://gitlab.com/derfreak/MediaScrapper/commit/fd65f8d22213f9cbf1ad68d41f942724157728c3))

## [3.0.4](https://gitlab.com/derfreak/MediaScrapper/compare/v3.0.3...v3.0.4) (2022-12-29)


### Bug Fixes

* crash using '--publish' with unknown movies ([f76798a](https://gitlab.com/derfreak/MediaScrapper/commit/f76798a47f4e3af8a201cfc396cccf77c4f61d34))

## [3.0.3](https://gitlab.com/derfreak/MediaScrapper/compare/v3.0.2...v3.0.3) (2022-12-29)


### Bug Fixes

* naming of scraper and storage folder ([88586a4](https://gitlab.com/derfreak/MediaScrapper/commit/88586a4093941b41dab7fabf2cb5083940e33591))
* resend unsent movies ([79cbd6b](https://gitlab.com/derfreak/MediaScrapper/commit/79cbd6ba059c9a5c35c9b639a3ba23ecdb9bc6c7))

## [3.0.2](https://gitlab.com/derfreak/MediaScrapper/compare/v3.0.1...v3.0.2) (2022-12-28)


### Bug Fixes

* poetry setup ([4e96098](https://gitlab.com/derfreak/MediaScrapper/commit/4e960987319640b607567751ca7f2f2c39c70a25))

## [3.0.1](https://gitlab.com/derfreak/MediaScrapper/compare/v3.0.0...v3.0.1) (2022-12-28)


### Bug Fixes

* semantic-release ([ea808a8](https://gitlab.com/derfreak/MediaScrapper/commit/ea808a834822e97bbd650720a877fec7e5c6e96c))

# [3.0.0](https://gitlab.com/derfreak/MediaScrapper/compare/v2.0.1...v3.0.0) (2022-12-28)


### Bug Fixes

* call application from cli without python -m ([cfbb231](https://gitlab.com/derfreak/MediaScrapper/commit/cfbb2318122c2b73aad386a8190e2bd8e3a2d9f7))
* install toml before semantic release ([024e013](https://gitlab.com/derfreak/MediaScrapper/commit/024e0134e8248da17f77cf555fd904211733c22f))
* semantic release ([6af98e1](https://gitlab.com/derfreak/MediaScrapper/commit/6af98e19f6f88c7be4c1fa5cae6a55927494f725))
* semantic-release ([339e090](https://gitlab.com/derfreak/MediaScrapper/commit/339e09061d9d2565ab26f704dad463bc9466b02d))


### Features

* rename to MendiaFileScraper ([bf13296](https://gitlab.com/derfreak/MediaScrapper/commit/bf1329615c26440b04c6b56602959649578b9f0e))
* show new movies in terminal ([ecd406f](https://gitlab.com/derfreak/MediaScrapper/commit/ecd406f8b0e64a054687fd18775aeaf9219941c4))


### BREAKING CHANGES

* The directory where the data is stored was renamed from
`.networkfilescrapper` to `.MendiaFileScraper`.

## [2.0.1](https://gitlab.com/derfreak/MediaScrapper/compare/v2.0.0...v2.0.1) (2022-12-27)


### Bug Fixes

* do not ignore '--publish' for movies ([fbe0c94](https://gitlab.com/derfreak/MediaScrapper/commit/fbe0c94d983b1a41efe6feb3d1c69ffa32003250))
* fix config file for the websocket connection ([aa1c157](https://gitlab.com/derfreak/MediaScrapper/commit/aa1c1576801f7624f7ae5b8da84f4c8ba7a90491))
* show unknown audo and subtitle tracks ([a086f35](https://gitlab.com/derfreak/MediaScrapper/commit/a086f35ad13249fa0707f07b2802cc2918204333))

# [2.0.0](https://gitlab.com/derfreak/MediaScrapper/compare/v1.1.2...v2.0.0) (2022-12-18)


### Features

* send movies over a websocket connection ([7bddb74](https://gitlab.com/derfreak/MediaScrapper/commit/7bddb741cbcc0c163f9105cdbd122d0066ac056a))


### BREAKING CHANGES

* This feature can only be used with the new websocket
based server application Mendia (https://gitlab.com/derfreak/mendia).

## [1.1.2](https://gitlab.com/derfreak/MediaScrapper/compare/v1.1.1...v1.1.2) (2021-09-11)


### Bug Fixes

* retry https connection requests on error ([f47c50a](https://gitlab.com/derfreak/MediaScrapper/commit/f47c50ac20082c8e39287085ba87c126c255a1e8)), closes [#10](https://gitlab.com/derfreak/MediaScrapper/issues/10)

## [1.1.1](https://gitlab.com/derfreak/MediaScrapper/compare/v1.1.0...v1.1.1) (2021-01-01)


### Bug Fixes

* improve bitrate determination ([cee21da](https://gitlab.com/derfreak/MediaScrapper/commit/cee21da9618cdda676458d6909463b35be403ff2)), closes [#8](https://gitlab.com/derfreak/MediaScrapper/issues/8)
* improve hdr detection ([cbf2a59](https://gitlab.com/derfreak/MediaScrapper/commit/cbf2a59509bf06b400fd591a85ebe4fba285c206)), closes [#7](https://gitlab.com/derfreak/MediaScrapper/issues/7)
* make video_ts code work on more systems ([f6d164d](https://gitlab.com/derfreak/MediaScrapper/commit/f6d164de8cb2eb2100bb3055a5bca6e0ffdf8c06)), closes [#5](https://gitlab.com/derfreak/MediaScrapper/issues/5)

# [1.1.0](https://gitlab.com/derfreak/MediaScrapper/compare/v1.0.0...v1.1.0) (2020-12-31)


### Features

* **movies:** support for VIDEO_TS folders ([b7ddc06](https://gitlab.com/derfreak/MediaScrapper/commit/b7ddc06aea5cef8c971caae096a47a33373a45c4)), closes [#5](https://gitlab.com/derfreak/MediaScrapper/issues/5)

# 1.0.0 (2020-12-30)


### Bug Fixes

* fix multiple spaces ([076fcf7](https://gitlab.com/derfreak/MediaScrapper/commit/076fcf798c5131e6b02f08c4c3d2593684a14fcb))
* name gitignore correct ([18e768b](https://gitlab.com/derfreak/MediaScrapper/commit/18e768be0645239b248beef59c96b59215435ab9))


### Features

* create and populate config.txt ([59453e5](https://gitlab.com/derfreak/MediaScrapper/commit/59453e528bfad63ae4057e74636abee90382c486))
* hash and scan movies ([acf6cc0](https://gitlab.com/derfreak/MediaScrapper/commit/acf6cc04510767278e890bd886ee9620db98b54e))
* mediainfo ([4826a5a](https://gitlab.com/derfreak/MediaScrapper/commit/4826a5a88f3c37d5bb57c5a93471fdd918ff8e93))
* speedup hashing and fix year parsing with regex ([14188bc](https://gitlab.com/derfreak/MediaScrapper/commit/14188bcb914ce4e763bdb4172d62e9df2db0a5b3))
* use logging ([46712f7](https://gitlab.com/derfreak/MediaScrapper/commit/46712f786bd8249d80e466442b5be774621530b3))
