FROM debian:buster
COPY . /app
RUN apt-get update
RUN apt-get install -y python3.7 python3-pip libmediainfo0v5
RUN python3.7 -m pip install /app
